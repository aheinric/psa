import dis
import copy
import pprint

NO_MOD_STACK = set([
  "NOP",
  "CONTINUE_LOOP",
  "BREAK_LOOP",
  "RETURN_VALUE",
  "RAISE_VARARGS",
  "END_FINALLY",
  "DELETE_ATTR",
  "DELETE_GLOBAL",
  "DELETE_NAME",
  "JUMP_FORWARD",
  "JUMP_ABSOLUTE",
  "DELETE_FAST"
  ])

MODS_LOCALS = list([
  "STORE_FAST",
  "DELETE_FAST",
  ])

MODS_GLOBALS = list([
  "STORE_GLOBAL",
  "DELETE_GLOBAL",
  ])

def run(cfgs):
  computeStackChanges(cfgs)
  computeVarChanges(cfgs, "locals", MODS_LOCALS)
  computeVarChanges(cfgs, "globals", MODS_GLOBALS)
  computePhi(cfgs)

def computeStackChanges(cfgs):
  for name in cfgs:
    cfg = cfgs[name][2]
    for x in cfg:
      count = 0;
      newCode = list()
      prevState = dict()
      for ins in cfg[x]["code"]:
        newState = dict()
        oldState = prevState
        if ins[2] not in NO_MOD_STACK:
          newState["stack"] = set([name + ":" + str(count)])
          count = count + 1
        elif "stack" in oldState:
          newState["stack"] = set(oldState["stack"])
        newCode.append(ins + (oldState, newState))
        prevState = newState
      cfg[x]["code"] = newCode
      cfg[x]["lastState"] = prevState

def computeVarChanges(cfgs, keyword, func_spec):
  for name in cfgs:
    cfg = cfgs[name][2]
    counts = dict();
    for x in cfg:
      for ins in cfg[x]["code"]:
        if keyword not in ins[6]:
          ins[6][keyword] = dict()
        if ins[2] in func_spec:
          if ins[4] not in counts:
            counts[ins[4]] = 0
          ins[6][keyword][ins[4]] = set([name + ":" + str(counts[ins[4]])])
          counts[ins[4]] = counts[ins[4]] + 1
        if keyword in ins[5]:
          for var in ins[5][keyword]:
            if var not in ins[6][keyword]:
              ins[6][keyword][var] = set(ins[5][keyword][var])

def computePhi(cfgs):
  out = dict()
  for name in cfgs:
    cfg = cfgs[name][2]
    preds = dict()
   
    lastInputs = None
    inputs = dict()
    for x in cfg:
      inputs[x] = dict()
      inputs[x]["locals"] = dict()
      inputs[x]["globals"] = dict()
    # Compute predecessors
    for x in cfg:
      for y in cfg[x]["targets"]:
        if y not in preds:
          preds[y] = set()
        preds[y].add(x)
    for x in cfg:
      if x not in preds:
        preds[x] = set()

    # Compute reaching definition for states
    while cmp(lastInputs, inputs) is not 0:
      lastInputs = inputs
      inputs = copy.deepcopy(inputs)
      for x in cfg:
        for y in preds[x]:
          #Handle the stack
          if "stack" not in inputs[x]:
            inputs[x]["stack"] = set()
          if "stack" in cfg[y]["lastState"]:
            inputs[x]["stack"] |= cfg[y]["lastState"]["stack"]
          elif "stack" in inputs[y]:
            inputs[x]["stack"] |= inputs[y]["stack"]
          
          # Handle locals
          if "locals" in cfg[y]["lastState"]:
            for var in cfg[y]["lastState"]["locals"]:
              if var not in inputs[x]["locals"]:
                inputs[x]["locals"][var] = set()
              inputs[x]["locals"][var] |= cfg[y]["lastState"]["locals"][var]
          for var in inputs[y]["locals"]:
            if "locals" not in cfg[y]["lastState"] or var not in cfg[y]["lastState"]["locals"]:
              if var not in inputs[x]["locals"]:
                inputs[x]["locals"][var] = set()
              inputs[x]["locals"][var] |= inputs[y]["locals"][var]
          
          # Handle globals
          if "globals" in cfg[y]["lastState"]:
            for var in cfg[y]["lastState"]["globals"]:
              if var not in inputs[x]["globals"]:
                inputs[x]["globals"][var] = set()
              inputs[x]["globals"][var] |= cfg[y]["lastState"]["globals"][var]
          for var in inputs[y]["globals"]:
            if "globals" not in cfg[y]["lastState"] or var not in cfg[y]["lastState"]["globals"]:
              if var not in inputs[x]["globals"]:
                inputs[x]["globals"][var] = set()
              inputs[x]["globals"][var] |= inputs[y]["globals"][var]
    for x in cfg:
      for ins in cfg[x]["code"]:
        for i in range(5, 7):
          if "stack" not in ins[i] and "stack" in inputs[x]:
            ins[i]["stack"] = inputs[x]["stack"]

          if "locals" not in ins[i] and "locals" in inputs[x]:
            ins[i]["locals"] = inputs[x]["locals"]
          elif "locals" in inputs[x]:
            for var in inputs[x]["locals"]:
              if var not in ins[i]["locals"]:
                ins[i]["locals"][var] = inputs[x]["locals"][var]

          if "globals" not in ins[i]:
            ins[i]["globals"] = inputs[x]["globals"]
          else:
            for var in inputs[x]["globals"]:
              if var not in ins[i]["globals"]:
                ins[i]["globals"][var] = inputs[x]["globals"][var]
          
