import dis
import inspect
import sys

EXTENDED_ARG = dis.opmap["EXTENDED_ARG"]

def run(names):
  out = dict()
  for name in names:
    out.update(handleModule(name))
  return out

def handleModule(module):
  cfgMap = dict()
  for (name, func) in getFunctionsOfModule(sys.modules[module]):
    cfgMap[module + "." + name] = (sys.modules[module], func, constructCFG(internalDis(func, bytestringToArray(func.func_code.co_code))))
  return cfgMap

def getFunctionsOfModule(module):
  return [(name, func) for (name, func) in inspect.getmembers(module, inspect.isfunction)]

def bytestringToArray(bytecode):
  return [ord(x) for x in bytecode]

def internalDis(func, byteArray):
  opArray = list()
  length = len(byteArray)
  while len(byteArray) is not 0:
    offset = length - len(byteArray)
    opArray.append(handleOpcode(func, byteArray, offset))
  return opArray

#Process a bytecode instruction into a useable datastructure
#Also handles recovering and reconstructing the arguments.
def handleOpcode(func, byteArray, offset, extra=None):
  opcode = byteArray.pop(0)
  arg = -1
  argS = ""

  if opcode >= dis.HAVE_ARGUMENT:
    arg = byteArray.pop(0)
    arg = (byteArray.pop(0) << 8) | arg
  if extra is not None:
    arg = (extra << 16) | arg

  if opcode == EXTENDED_ARG:
    return handleOpcode(func, byteArray, offset + 3, arg)
  
  if dis.hasconst.count(opcode) is not 0:
    argS = func.func_code.co_consts[arg]
  elif dis.hasfree.count(opcode) is not 0:
    argS = func.func_code.co_varnames[arg]
  elif dis.hasname.count(opcode) is not 0:
    argS = func.func_code.co_names[arg]
  elif dis.haslocal.count(opcode) is not 0:
    argS = func.func_code.co_varnames[arg]

  # Format: offset, opcode, opname, arg, pretty arg, beginning state, end state
  return (offset, opcode, dis.opname[opcode], arg, argS)

def constructCFG(opArray):
  bblockArray = dict()
  lastCount = -1
  count = 0
  for (offset, opcode, opname, arg, argS) in opArray:
    if opname is "RETURN_VALUE" or opname is "YIELD_VALUE" or opname is "RAISE_VARARGS":
      bblock = dict()
      bblock["start"] = opArray[lastCount + 1][0]
      bblock["targets"] = list()
      bblock["code"] = opArray[lastCount + 1: count + 1]
      bblockArray[bblock["start"]] = bblock
      lastCount = count
    if dis.hasjrel.count(opcode) is not 0:
      bblock = dict()
      bblock["start"] = opArray[lastCount + 1][0]
      bblock["targets"] = list()
      if opname is "FOR_ITER":
        bblock["targets"].append(opArray[count + 1][0] + arg)
      else:
        bblock["targets"].append(opArray[count + 1][0] + arg - 1)
      if opname is not "JUMP_FORWARD":
        bblock["targets"].append(opArray[count + 1][0])
      bblock["code"] = opArray[lastCount + 1: count + 1]
      bblockArray[bblock["start"]] = bblock
      lastCount = count
    elif dis.hasjabs.count(opcode) is not 0:
      bblock = dict()
      bblock["start"] = opArray[lastCount + 1][0]
      bblock["targets"] = list()
      bblock["targets"].append(arg)
      if opname is not "JUMP_ABSOLUTE":
        bblock["targets"].append(opArray[count + 1][0])
      bblock["code"] = opArray[lastCount + 1: count + 1]
      bblockArray[bblock["start"]] = bblock
      lastCount = count
    count = count + 1 
  return bblockArray

