#! /usr/bin/python
import cfg_constructor
import ssa_converter
import cfg_dataflow
import pprint
# Add the modules you wish to analyze to this list.

def main():
  print "Starting CFG construction"
  #Replace the argument with a list of module names you wish to analyze
  cfgs = cfg_constructor.run(["cfg_constructor"])
  ssa_converter.run(cfgs)
  summaries = cfg_dataflow.run(cfgs)
  doit = True
  for name in cfgs:
    print name
    ## Print
    #printCFG(cfgs[name])
    ## Print the taints present in all single-static-assignment states
    #pprint.pprint(summaries[name][0])
    ## Print the sinks detected by the program, and the sources that taint them.
    #pprint.pprint(summaries[name][1])
    ## Print the errors reported by the program
    #pprint.pprint(summaries[name[2])
def printCFG(cfgTuple):
  (module, func, cfg) = cfgTuple
  for x in cfg:
    bblock = cfg[x]
    print "\tStart: " + str(bblock["start"])
    print "\tTargets: " + str(bblock["targets"])
    print "\tCode: "
    for y in bblock["code"]:
      print "\t\t" + str(y[0]) + " " + str(y[1]) + " " + str(y[2]) + " " + str(y[3]) + " " + str(y[4])
      print "\t\tPrev State:"
      if "stack" in y[5]:
        print "\t\t\tStack: " + str(y[5]["stack"])
      if "locals" in y[5]:
        print "\t\t\tLocals: "
        for z in y[5]["locals"]:
          print "\t\t\t\t" + str(z) + ": " + str(y[5]["locals"][z]) 
      if "globals" in y[5]:
        print "\t\t\tGlobals: "
        for z in y[5]["globals"]:
          print "\t\t\t\t" + str(z) + ": " + str(y[5]["globals"][z])
  
      print "\t\tNext State:"
      if "stack" in y[6]:
        print "\t\t\tStack: " + str(y[6]["stack"])
      if "locals" in y[6]:
        print "\t\t\tLocals: "
        for z in y[6]["locals"]:
          print "\t\t\t\t" + str(z) + ": " + str(y[6]["locals"][z]) 
      if "globals" in y[6]:
        print "\t\t\tGlobals: "
        for z in y[6]["globals"]:
          print "\t\t\t\t" + str(z) + ": " + str(y[6]["globals"][z])

if __name__ == "__main__":
  main()
