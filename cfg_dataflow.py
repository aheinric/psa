import dis
import copy

def run(cfgs):
  summaries = dict()
  for name in cfgs:
    cfg = cfgs[name][2]
    summaries[name] = summarize(name, cfg)
  return summaries

def summarize(name, cfg):
  oldStates = None
  states = dict()
  oldObjects = None
  objects = dict()
  oldSinks = None
  sinks = set()
  errors = set()
  while cmp(oldStates, states) is not 0 and (oldSinks != sinks) and cmp(oldObjects, objects) is not 0:
    print "Iterating on " + name
    oldStates = states
    states = copy.deepcopy(states)
    oldSinks = sinks
    sinks = copy.deepcopy(sinks)
    oldObjects = objects
    objects = copy.deepcopy(objects)
    for x in cfg:
      for ins in cfg[x]["code"]:
        execute(ins, states, objects, sinks, errors)
  return (states, objects, errors)

def report_error(ins, msg, errors):
  errors.add(str(ins[0:4]) + ": " + msg)

def execute(ins, states, objects, sinks, errors):
  opname = ins[2]
  if opname is "NOP":
    return
    #Nop does nothing
  elif opname is "POP_TOP":
    execute_POP_TOP(ins, states, errors)
  elif opname is "ROT_TWO":
    execute_ROT(2, ins, states, errors)
  elif opname is "ROT_THREE":
    execute_ROT(3, ins, states, errors)
  elif opname is "ROT_FOUR":
    execute_ROT(4, ins, states, errors)
  elif opname is "DUP_TOP":
    execute_DUP_TOPX(1, ins, states, errors)
  elif opname is "UNARY_POSITIVE":
    execute_UNARY_OP(ins, states, errors)
  elif opname is "UNARY_NEGATIVE":
    execute_UNARY_OP(ins, states, errors)
  elif opname is "UNARY_NOT":
    execute_UNARY_OP(ins, states, errors)
  elif opname is "UNARY_CONVERT":
    execute_UNARY_OP(ins, states, errors)
  elif opname is "UNARY_INVERT":
    execute_UNARY_OP(ins, states, errors)
  elif opname is "GET_ITER":
    # Since we're treating sequences as single objects, this is just unary.
    execute_UNARY_OP(ins, states, errors)
  elif opname is "BINARY_POWER":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_MULTIPLY":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_FLOOR_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_TRUE_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_MODULO":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_ADD":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_SUBTRACT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_SUBSCR":
    #TODO I believe this is just sequence types?
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_LSHIFT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_RSHIFT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_AND":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_XOR":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "BINARY_OR":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_POWER":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_MULTIPLY":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_FLOOR_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_TRUE_DIVIDE":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_MODULO":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_ADD":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_SUBTRACT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_LSHIFT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_RSHIFT":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_AND":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_XOR":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "INPLACE_OR":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "SLICE+0":
    execute_SLICE_OP(0, ins, states, errors)
  elif opname is "SLICE+1":
    execute_SLICE_OP(1, ins, states, errors)
  elif opname is "SLICE+2":
    execute_SLICE_OP(2, ins, states, errors)
  elif opname is "SLICE+3":
    execute_SLICE_OP(3, ins, states, errors)
  elif opname is "STORE_SLICE+0":
    execute_SLICE_OP(0, ins, states, errors)
  elif opname is "STORE_SLICE+1":
    execute_SLICE_OP(1, ins, states, errors)
  elif opname is "STORE_SLICE+2":
    execute_SLICE_OP(2, ins, states, errors)
  elif opname is "STORE_SLICE+3":
    execute_SLICE_OP(3, ins, states, errors)
  elif opname is "DELETE_SLICE+0":
    # TODO Are these the same as other slices?
    execute_SLICE_OP(0, ins, states, errors)
  elif opname is "DELETE_SLICE+1":
    execute_SLICE_OP(1, ins, states, errors)
  elif opname is "DELETE_SLICE+2":
    execute_SLICE_OP(2, ins, states, errors)
  elif opname is "DELETE_SLICE+3":
    execute_SLICE_OP(3, ins, states, errors)
  elif opname is "STORE_SUBSCR":
    #TODO I believe this is just sequence types?
    execute_BINARY_OP(ins, states, errors)
  elif opname is "DELETE_SUBSCR":
    #TODO I believe this is just sequence types?
    execute_BINARY_OP(ins, states, errors)
  elif opname is "PRINT_ITEM":
    execute_PRINT_ITEM(ins, states, sinks, errors)
  elif opname is "PRINT_ITEM_TO":
    execute_PRINT_ITEM_TO(ins, states, sinks, errors)
  elif opname is "PRINT_NEWLINE":
    # This is effectively a nop
    return
  elif opname is "PRINT_NEWLINE_TO":
    # I _think_ this is effectively a nop
    return
  elif opname is "BREAK_LOOP":
    # CF only
    return
  elif opname is "CONTINUE_LOOP":
    # CF only
    return
  elif opname is "LIST_APPEND":
    #TODO I have no idea what this even means.
    report_error(ins, "LIST_APPEND is not modeled.  It deals with block scope", errors)
  elif opname is "LOAD_LOCALS":
    execute_LOAD_LOCALS(ins, states, errors)
  elif opname is "RETURN_VALUE":
    execute_RETURN(ins, states, sinks, errors)
  elif opname is "YIELD_VALUE":
    execute_YIELD(ins, states, sinks, errors)
  elif opname is "IMPORT_STAR":
    execute_IMPORT_STAR(ins, states, objects, errors)
  elif opname is "EXEC_STMT":
    report_error(ins, "EXEC_STMT is not modeled. It deals with arbitrary code.", errors)
  elif opname is "POP_BLOCK":
    report_error(ins, "POP_BLOCK is not modeled. It deals with block scope.", errors)
  elif opname is "END_FINALLY":
    report_error(ins, "END_FINALLY is not modeled. It deals with block scope.", errors)
  elif opname is "BUILD_CLASS":
    report_error(ins, "BUILD_CLASS is not modeled.  It deals with block scope.", errors)
  elif opname is "WITH_CLEANUP":
    report_error(ins, "WITH_CLEANUP is not modeled precisely.", errors)
    execute_WITH_CLEANUP(ins, states, errors)
  elif opname is "STORE_NAME":
    report_error(ins, "STORE_NAME is not modeled; not possible to determine its impact", errors)
  elif opname is "DELETE_NAME":
    report_error(ins, ": DELETE_NAME is not modeled; not possible to determine its impact", errors)
  elif opname is "UNPACK_SEQUENCE":
    execute_DUP_TOPX(ins[3], ins, states, errors)
  elif opname is "DUP_TOPX":
    execute_DUP_TOPX(ins[3], ins, states, errors)
  elif opname is "STORE_ATTR":
    execute_STORE_ATTR(ins, states, objects, errors)
  elif opname is "DELETE_ATTR":
    report_error(ins, "DELETE_ATTR is not modeled.  Objects are context insensitive", errors)
  elif opname is "STORE_GLOBAL":
    execute_STORE_GLOBAL(ins, states, errors)
  elif opname is "DELETE_GLOBAL":
    # SSA means this is actually a no-op; by not transfering taint, we kill it.
    return
  elif opname is "LOAD_CONST":
    execute_LOAD_CONST(ins, states, errors)
  elif opname is "LOAD_NAME":
    report_error(ins, "LOAD_NAME is not modeled.  Not possible to determine its impact", errors)
  elif opname is "BUILD_TUPLE":
    execute_BUILD_TUPLE(ins, states, errors)
  elif opname is "BUILD_LIST":
    execute_BUILD_TUPLE(ins, states, errors)
  elif opname is "BUILD_MAP":
    execute_BUILD_MAP(ins, states, errors)
  elif opname is "LOAD_ATTR":
    execute_LOAD_ATTR(ins, states, objects, errors)
  elif opname is "COMPARE_OP":
    execute_BINARY_OP(ins, states, errors)
  elif opname is "IMPORT_NAME":
    execute_IMPORT_NAME(ins, states, errors)
  elif opname is "IMPORT_FROM":
    execute_IMPORT_FROM(ins, states, objects, errors)
  elif opname is "JUMP_FORWARD":
    # CF only
    return
  elif opname is "POP_JUMP_IF_TRUE":
    execute_POP_TOP(ins, states, errors)
  elif opname is "POP_JUMP_IF_TRUE":
    execute_POP_TOP(ins, states, errors)
  elif opname is "JUMP_IF_TRUE_OR_POP":
    report_error(ins, "The JUMP_*_OR_POP family is a pain, and not modeled correctly.", errors)
    execute_POP_TOP(ins, states, errors)
  elif opname is "JUMP_IF_TRUE_OR_POP":
    report_error(ins, "The JUMP_*_OR_POP family is a pain, and not modeled correctly.", errors)
    execute_POP_TOP(ins, states, errors)
  elif opname is "JUMP_ABSOLUTE":
    # CF only
    return
  elif opname is "FOR_ITER":
    execute_FOR_ITER(ins, states, errors)
  elif opname is "LOAD_GLOBAL":
    execute_LOAD_GLOBAL(ins, states, errors)
  elif opname is "SETUP_LOOP":
    # CF only, mod block scope I'm not dealing with.
    return
  elif opname is "SETUP_EXCEPT":
    # CF only, mod block scope I'm not dealing with.
    return
  elif opname is "SETUP_FINALLY":
    # CF only, mod block scope I'm not dealing with.
    return
  elif opname is "STORE_MAP":
    execute_STORE_ATTR(ins, states, objects, errors)
  elif opname is "LOAD_FAST":
    execute_LOAD_FAST(ins, states, errors)
  elif opname is "STORE_FAST":
    execute_STORE_FAST(ins, states, errors)
  elif opname is "DELETE_FAST":
    # SSA means this is actually a no-op; by not transfering taint, we kill it.
    return
  elif opname is "LOAD_CLOSURE":
    # TODO Would need to see an example of this.
    report_error(ins, "LOAD_CLOSURE is not handled.  I don't know what it does", errors)
  elif opname is "LOAD_DEREF":
    # TODO Would need to see an example of this.
    report_error(ins, "LOAD_DEREF is not handled.  I don't know what it does", errors)
  elif opname is "STORE_DEREF":
    # TODO Would need to see an example of this.
    report_error(ins, "STORE_DEREF is not handled.  I don't know what it does", errors)
  elif opname is "RAISE_VARARGS":
    execute_RAISE_VARARGS(ins, states, sinks, errors)
  elif opname is "CALL_FUNCTION":
    execute_CALL_FUNCTION(ins, states, sinks, errors)
  elif opname is "MAKE_FUNCTION":
    report_error(ins, "MAKE_FUNCTION is not handled.  Arbitrary code execution", errors)
  elif opname is "MAKE_CLOSURE":
    report_error(ins, "MAKE_CLOSURE is not handled.  Arbitrary code execution", errors)
  elif opname is "BUILD_SLICE":
    execute_SLICE_OP(ins[3], ins, states, errors)
  elif opname is "CALL_FUNCTION_VAR":
    execute_CALL_FUNCTION_VAR(ins, states, sinks, errors)
  elif opname is "CALL_FUNCTION_KW":
    execute_CALL_FUNCTION_KW(ins, states, sinks, errors)
  elif opname is "CALL_FUNCTION_VAR_KW":
    execute_CALL_FUNCTION_VAR_KW(ins, states, sinks, errors)


def sinkStack(label, stackIdx, ins, states, sinks, errors):
  if "stack" not in ins[5]:
    # Error condition: We need a stack state to get our sources from.
    report_error(ins, "Bad sink stack; we need a stack to sink from", errors)
    return
  prevStates = ins[5]["stack"]
  for x in prevStates:
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    if stackIdx in prevState:
      sinkData(label, prevState[stackIdx], ins, sinks, errors)

def sinkData(label, sources, ins, sinks, errors):
  for source in sources:
    sinks.add((label, ins[0], source))


def pushNewSource(kind, name, ins, states, errors):
  pushData(set([(kind, name, ins[0])]), ins, states, errors)

def pushData(data, ins, states, errors):
  if "stack" not in ins[6]:
    # Error condition: We need a stack state to sink our source
    report_error(ins, "Bad push source; need a successor stack", errors)
    return
  newStates = ins[6]["stack"]
  if len(newStates) is not 1:
    # Error condition: There should only be one end state.
    report_error(ins, "Bad push source; we should SSA to exactly one end state", errors)
    return
  oldState = next(iter(newStates))
  if oldState not in states:
    states[oldState] = dict()
  oldState = states[oldState]
  newStack = dict()
  if "stack" in oldState:
    oldStack = oldState["stack"]
    for i in oldStack:
      newStack[i + 1] = oldStack[i]
  newStack[0] = set()
  newStack[0] |= data
  oldState["stack"] = newStack 

def execute_POP_TOP(ins, states, error):
  execute_POP_NTH(0, ins, states, error)

def execute_POP_NTH(n, ins, states, error):
  if "stack" not in ins[5] or "stack" not in ins[6]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad POP_NTH; need stack state on both sides", errors)
    return
  prevStates = ins[5]["stack"]
  newStates = ins[6]["stack"]
  if len(newStates) is not 1:
    # Error condition: We should have only one end state
    report_error(ins, "Bad POP_NTH; we should SSA to exactly one end state", errors) 
    return
  newState = next(iter(newStates))
  if newState not in states:
    states[newState] = dict()
  newState = states[newState]
  if "stack" not in newState:
    newState["stack"] = dict()
  for x in prevStates:
    if x in newStates:
      # Error condition: If  stack state transitions into itself, this won't terminate.
      report_error(ins, "Bad POP_NTH; we we can't transfer from a state to itself", errors)
      return
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    for i in prevState["stack"]:
      newI = i
      if i is n:
        continue
      if i > n:
        newI = i - 1
      if newI not in newState["stack"]:
        newState["stack"][newI] = set()
      newState["stack"][newI] |= prevState["stack"][i]


def execute_ROT(n, ins, states, errors):
  if "stack" not in ins[5] or "stack" not in ins[6]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad ROT; need stack state on both sides", errors)
    return
  prevStates = ins[5]["stack"]
  newStates = ins[6]["stack"]
  if len(newStates) is not 1:
    # Error condition: We should have only one end state
    report_error(ins, "Bad ROT; we should SSA to exactly one end state", errors) 
    return
  newState = next(iter(newStates))
  if newState not in states:
    states[newState] = dict()
  newState = states[newState]
  if "stack" not in newState:
    newState["stack"] = dict()
  for x in prevStates:
    if x in newStates:
      # Error condition: If stack state transitions into itself, this won't terminate.
      report_errors(ins, "Bad ROT; we we can't transfer from a state to itself", errors)
      return
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    for i in prevState["stack"]:
      newI = i
      if i is 0:
        newI = n - 1
      if i < n:
        newI = i - 1
      if newI not in newState["stack"]:
        newState["stack"][newI] = set()
      newState["stack"][newI] |= prevState["stack"][i]

def execute_DUP_TOPX(n, ins, states, errors):
  if "stack" not in ins[5]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad DUP_TOP; need stack state for prev", errors)
    return
  if "stack" not in ins[6]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad DUP_TOP; need stack state for next", errors)
    return
  prevStates = ins[5]["stack"]
  newStates = ins[6]["stack"]
  if len(newStates) is not 1:
    # Error condition: We should have only one end state
    report_error(ins, "Bad DUP_TOP; we should SSA to exactly one end state", errors) 
    return
  newState = next(iter(newStates))
  if newState not in states:
    states[newState] = dict()
  newState = states[newState]
  if "stack" not in newState:
    newState["stack"] = dict()
  for x in prevStates:
    if x in newStates:
      # Error condition: If stack state transitions into itself, this won't terminate.
      report_errors(ins, "Bad DUP_TOP; we we can't transfer from a state to itself", errors)
      return
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    for i in prevState["stack"]:
      newI = i
      if i is 0:
        for newI in range(0, n):
          if newI not in newState["stack"]:
            newState["stack"][newI] = set()
          newState["stack"][newI] |= prevState["stack"][i]
      else:
        newI = i + n
        if newI not in newState["stack"]:
          newState["stack"][newI] = set()
        newState["stack"][newI] |= prevState["stack"][i]

def execute_UNARY_OP(ins, states, errors):
  executeStackCollapse(1, ins, states, errors)

def execute_BINARY_OP(ins, states, errors):
  executeStackCollapse(2, ins, states, errors)

def execute_SLICE_OP(n, ins, states, errors):
  executeStackCollapse(n + 1, ins, states, errors)

def executeStackCollapse(n, ins, states, errors):
  if "stack" not in ins[5]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad collapse op; need stack state for prev.", errors)
    return
  if "stack" not in ins[6]:
    # Error condition; we need _something_ on the stack.
    report_error(ins, "Bad collapse op; need stack state for next", errors)
    return
  prevStates = ins[5]["stack"]
  newStates = ins[6]["stack"]
  if len(newStates) is not 1:
    # Error condition: We should have only one end state
    report_error(ins, "Bad collapse op; we should SSA to exactly one end state", errors) 
    return
  newState = next(iter(newStates))
  if newState not in states:
    states[newState] = dict()
  newState = states[newState]
  if "stack" not in newState:
    newState["stack"] = dict()
  for x in prevStates:
    if x in newStates:
      # Error condition: If stack state transitions into itself, this won't terminate.
      report_errors(ins, "Bad collapse op; we we can't transfer from a state to itself", errors)
      return
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    for i in prevState["stack"]:
      newI = i
      if i < n:
        newI = 0
      else:
        newI = i - n + 1
      if newI not in newState["stack"]:
        newState["stack"][newI] = set()
      newState["stack"][newI] |= prevState["stack"][i]

def execute_PRINT_ITEM(ins, states, sinks, errors):
  execute_POP_TOP(ins, states, errors)
  sinkStack("print output", 0, ins, states, sinks, errors)

def execute_PRINT_ITEM_TO(ins, states, sinks, errors):
  execute_POP_NTH(1, ins, states, errors)
  sinkStack("print dest", 0, ins, states, sinks, errors)
  sinkStack("print output", 1, ins, states, sinks, errors)

def execute_RETURN(ins, states, sinks, errors):
  sinkStack("return", 0, ins, states, sinks, errors)

def execute_YIELD(ins, states, sinks, errors):
  execute_POP_TOP(ins, states, errors)
  sinkStack("return", 0, ins, states, sinks, errors)

def execute_IMPORT_STAR(ins, states, objects):
  report_error(ins, "IMPORT_STAR is not modeled.  This may have adverse impacts on results.")

def execute_WITH_CLEANUP(ins, states):
  report_error(ins, "WITH_CLEANUP is not modeled.  This may have adverse impacts on results.")

def execute_STORE_ATTR(ins, states, objects):
  execute_POP_NTH(1, ins, states, errors) 
  prevStates = ins[5]
  if "stack" not in ins[5]:
    # Error case: we need a stack
    report_error(ins, "Bad LOAD_ATTR; we're missing a stack", errors)
    return
  data = set()
  for x in ins[5]["stack"]:
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    prevStack = prevState["stack"]
    if 0 not in prevStack and 1 not in prevStack:
      continue
    objs = prevStack[0]
    taints = prevStack[1]
    for obj in objs:
      if obj[0] is "const":
        continue
      if obj not in objects:
        objects[obj] = dict()
      if ins[4] not in objects[obj]:
        objects[obj][ins[4]] = set()
        objects[obj].add(("initial", str(obj) + "." + ins[4], ins[0]))
      objects[obj] |= taints

def execute_LOAD_ATTR(ins, states, objects, errors):
  execute_POP_TOP(ins, states, errors)
  prevStates = ins[5]
  if "stack" not in ins[5]:
    # Error case: we need a stack
    report_error(ins, "Bad LOAD_ATTR; we're missing a stack", errors)
    return
  data = set()
  for x in ins[5]["stack"]:
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    prevStack = prevState["stack"]
    if 0 not in prevStack:
      continue
    objs = prevStack[0]
    for obj in objs:
      if obj[0] is "const":
        continue
      if obj not in objects:
        objects[obj] = dict()
      if ins[4] not in objects[obj]:
        objects[obj][ins[4]] = set()
        objects[obj][ins[4]].add(("initial", str(obj) + "." + ins[4], ins[0]))
      data |= objects[obj][ins[4]]
        
  pushData(data, ins, states, errors)
  return

def execute_BUILD_TUPLE(ins, states, errors):
  if ins[3] is 0:
    #Edge case; sometimes this op is called to create an empty tuple.
    pushNewSource("function", ins[2], ins, states, errors) 
  else:
    executeStackCollapse(ins[3], ins, states, errors)

def execute_BUILD_MAP(ins, states, errors):
  pushNewSource("return", "BUILD_MAP", ins, states, errors) 

def execute_IMPORT_NAME(ins, states, errors):
  report_error(ins, "IMPORT_NAME is not modeled.  This may have adverse impacts on the results", errors)

def execute_FOR_ITER(ins, states, errors):
  report_error(ins, "FOR_ITER's data flow is different for its control flow branches.  This isn't modeled correctly", errors)
  executeStackCollapse(1, ins, states, errors)
  return

def execute_STORE_VAR(keyword, ins, states, errors):
  #Model the fact that we pop top.
  execute_POP_TOP(ins, states, errors)
  # Find the previous stack
  if "stack" not in ins[5]:
    report_error(ins, "Bad " + ins[2] + "; we need a stack to transfer from", errors)
  prevStates = ins[5]["stack"]
  # Find the next named variable
  if keyword not in ins[6] or ins[4] not in ins[6][keyword]:
    report_error(ins, "Bad " + ins[2] + "; we need a " + str(ins[4]) + " to transfer to", errors)
  nextStates = ins[6][keyword][ins[4]]
  if len(nextStates) is not 1:
    report_error(ins, "Bad " + ins[2] + "; should only SSA to one state", errors)
  nextState = next(iter(nextStates))
  if nextState not in states:
    states[nextState] = dict()
  nextState = states[nextState]
  if keyword not in nextState:
    nextState[keyword] = dict()
  nextState = nextState[keyword]
  if ins[4] not in nextState:
    nextState[ins[4]] = set()
  nextState = nextState[ins[4]]
  for x in prevStates:
    if x not in states:
      states[x] = dict()
    prevState = states[x]
    if "stack" not in prevState:
      continue
    if 0 not in prevState["stack"]:
      prevState["stack"] = set()
    nextState |= prevState["stack"][0]

def execute_LOAD_VAR(keyword, ins, states, errors):
  # Find the previous named variable
  if keyword not in ins[5] or ins[4] not in ins[5][keyword]:
    pushNewSource("initial", ins[4], ins, states, errors)
  else:
    data = set()
    prevStates = ins[5][keyword][ins[4]]
    for x in prevStates:
      if x not in states:
        states[x] = dict()
      if keyword not in states[x] or ins[4] not in states[x][keyword]:
        continue
      data |= states[x][keyword][ins[4]]
    pushData(data, ins, states, errors)

  data = set()
  # Push its taints onto the stack
  pushData(data, ins, states, errors)

def execute_STORE_GLOBAL(ins, states, errors):
  execute_STORE_VAR("globals", ins, states, errors)

def execute_LOAD_GLOBAL(ins, states, errors):
  execute_LOAD_VAR("globals", ins, states, errors)

def execute_STORE_FAST(ins, states, errors):
  execute_STORE_VAR("locals", ins, states, errors) 

def execute_LOAD_FAST(ins, states, errors):
  execute_LOAD_VAR("locals", ins, states, errors)

def execute_RAISE_VARARGS(ins, states, sinks, errors):
  for i in range(0, ins[3]):
    sinkStack("exception", i, ins, states, sinks, errors)

def execute_LOAD_CONST(ins, states, errors):
  pushNewSource("const", ins[4], ins, states, errors)

def executeCallsite(args, argc, ins, states, sinks, errors):
  executeStackCollapse(argc, ins, states, errors)
  execute_POP_TOP(ins, states, errors)
  sinkStack("callsite func", argc - 1, ins, states, sinks, errors)
  for name in args:
    sinkData("callsite arg", args[name], ins, sinks, errors)
  pushNewSource("return", "callsite", ins, states, errors)

def execute_CALL_FUNCTION(ins, states, sinks, errors):
  keywordArgs = (ins[3] >> 8) & 0xFF
  realArgs = ins[3] & 0xFF
  args = dict()

  if "stack" not in ins[5]:
    # Error case: we need a stack
    report_error(ins, "Bad CALL_FUNCTION; missing a stack", errors)
    return
  prevStates = ins[5]["stack"]
  for x in prevStates:
    if x not in states:
      states[x] = dict()
    if "stack" not in states[x]:
      continue
    stack = states[x]["stack"]
    for i in range(0, keywordArgs):
      kwI = 2 * i
      argI = 2 * i + 1
      if kwI not in stack or argI not in stack:
        continue
      for name in stack[kwI]:
        if name not in args:
          args[name] = set()
        args[str(name)] |= stack[argI]
    for i in range(0, realArgs):
      argI = (2 * keywordArgs) + i
      if argI not in stack:
        continue
      args[str(i)] = stack[argI]

  executeCallsite(args, 2 * keywordArgs + realArgs, ins, states, sinks, errors)
def execute_CALL_FUNCTION_VAR(ins, states, sinks, errors):
  return
def execute_CALL_FUNCTION_KW(ins, states, sinks, errors):
  return
def execute_CALL_FUNCTION_VAR_KW(ins, states, sinks, errors):
  return
